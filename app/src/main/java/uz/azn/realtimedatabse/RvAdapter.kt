package uz.azn.realtimedatabse

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.realtimedatabse.databinding.RowItemBinding
import java.util.*

class RvAdapter : RecyclerView.Adapter<RvAdapter.RowItemViewHolder>() {

    private val elements = mutableListOf<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RowItemViewHolder {
        return RowItemViewHolder(
            RowItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return elements.size
    }

    override fun onBindViewHolder(holder: RowItemViewHolder, position: Int) {
        holder.onBind(elements[position])
    }

    inner class RowItemViewHolder(private val binding: RowItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind(element: User) {
            with(binding) {
                tvPassword.text = element.password
                tvUsername.text = element.username
            }
        }
    }

    fun setData(userElements: ArrayList<User>) {
        elements.apply { clear(); addAll(userElements) }
        notifyDataSetChanged()
    }
}
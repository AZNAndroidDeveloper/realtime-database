package uz.azn.realtimedatabse

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.database.*
import uz.azn.realtimedatabse.databinding.FragmentIntroBinding

class IntroFragment : Fragment(R.layout.fragment_intro) {
    private lateinit var binding: FragmentIntroBinding
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var databaseReference: DatabaseReference // qaysi malumotni qaysi holatda olib kelishni aniqlaydi

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIntroBinding.bind(view)
        var user_id: Long = 0
        firebaseDatabase = FirebaseDatabase.getInstance()
        databaseReference = firebaseDatabase.reference.child("user")
        // latest id before button click
        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {

            }

            override fun onDataChange(snapshot: DataSnapshot) {
                user_id = snapshot.childrenCount
            }

        })

        with(binding) {
            btnSave.setOnClickListener {
                databaseReference.addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(error: DatabaseError) {

                    }

                    override fun onDataChange(snapshot: DataSnapshot) {
                        if (snapshot.exists()) {
                            user_id = snapshot.childrenCount

                        }
                    }

                })
                val user = User(etUsername.text.toString(), etPassword.text.toString())
                databaseReference.child("user${++user_id}").setValue(user)
                Toast.makeText(requireContext(), "User saved", Toast.LENGTH_SHORT).show()

            }
            btnLoad.setOnClickListener {
                fragmentManager!!.beginTransaction().replace(R.id.frame_layout, LoadFragment()).commit()
            }
        }
    }

}
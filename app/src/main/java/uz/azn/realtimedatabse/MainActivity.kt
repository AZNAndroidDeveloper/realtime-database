package uz.azn.realtimedatabse

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import uz.azn.realtimedatabse.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private  val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
     supportFragmentManager.beginTransaction().replace(binding.frameLayout.id,IntroFragment()).commit()

    }
}